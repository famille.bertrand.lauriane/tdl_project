open Type

(* Interface des arbres abstraits *)
module type Ast =
sig
   type expression
   type instruction
   type df
   type programme
end

(* Interface d'affichage des arbres abstraits *)
module type PrinterAst =
sig
  module A:Ast

(* string_of_expression :  expression -> string *)
(* transforme une expression en chaîne de caractère *)
val string_of_expression : A.expression -> string

(* string_of_instruction :  instruction -> string *)
(* transforme une instruction en chaîne de caractère *)
val string_of_instruction : A.instruction -> string

(* string_of_df :  df -> string *)
(* transforme une fonction ou un prototype en chaîne de caractère *)
val string_of_df : A.df -> string

(* string_of_ast :  ast -> string *)
(* transforme un ast en chaîne de caractère *)
val string_of_programme : A.programme -> string

(* print_ast :  ast -> unit *)
(* affiche un ast *)
val print_programme : A.programme -> unit

end


(*******************************************)
(* AST après la phase d'analyse syntaxique *)
(*******************************************)
module AstSyntax =
struct

(* Opérateurs binaires de Rat *)
type binaire = Plus | Mult | Equ | Inf | Conc

(* Expressions de Rat *)
type expression =
  (* Appel de fonction représenté par le nom de la fonction et la liste des paramètres réels *)
  | AppelFonction of string * expression list 
  (* Rationnel représenté par le numérateur et le dénominateur *)
  | Rationnel of expression * expression 
  (* Accès au numérateur d'un rationnel *)
  | Numerateur of expression
  (* Accès au dénominateur d'un rationnel *)
  | Denominateur of expression
  (* Booléen vrai *)
  | True
  (* Booléen faux *)
  | False
  (* Entier *)
  | Entier of int
  (* Opération binaire représentée par l'opérateur, l'opérande gauche et l'opérande droite *)
  | Binaire of binaire * expression * expression
  (* Chaine de caractère *)
  | Chaine of string
  (* Accès à la sous chaine comprise entre deux indices d'une chaine *)
  | SousChaine of expression * expression * expression
  (* Accès à la longueur d'une chaine *)
  | Longueur of expression
  (* Null*)
  | Null
  (* Accès à l'adresse d'un identifiant représenté par son nom *)
  | Adresse of string
  (* Nouveau pointeur pour le type donné *)
  | New of typ
  (* Accès à un affectable  *)
  | Acces of affectable

(*Affectables de Rat*)
and affectable = 
  (* Accès à un identifiant représenté par son nom *)
  | Ident of string
  (*Accès à la valeur d'un affectable*)
  | Valeur of affectable

(* Instructions de Rat *)
type bloc = instruction list
and instruction =
  (* Déclaration de variable représentée par son type, son nom et l'expression d'initialisation *)
  | Declaration of typ * string * expression
  (* Affectation d'une variable représentée par son nom et la nouvelle valeur affectée *)
  | Affectation of affectable * expression
  (* Déclaration d'une constante représentée par son nom et sa valeur (entier) *)
  | Constante of string * int
  (* Affichage d'une expression *)
  | Affichage of expression
  (* Conditionnelle représentée par la condition, le bloc then et le bloc else *)
  | Conditionnelle of expression * bloc * bloc
  (*Boucle TantQue représentée par la condition d'arrêt de la boucle et le bloc d'instructions *)
  | TantQue of expression * bloc


(* Df : pour définir une fonction avec le corps (fonction) ou sans (prototype) *)
type df = 
  (* type de retour - nom - liste des paramètres (association type et nom) *)
  | Prototype of typ * string * (typ * string) list
  (* type de retour - nom - liste des paramètres (association type et nom) - bloc d'instruction - expression de retour *)
  | Fonction of typ * string * (typ * string) list * instruction list * expression

(* Structure pour les fonctions et prototypes *)
(* liste de df : fonctions ou prototypes*)
type dfs = Dfs of (df list) 

(* Structure d'un programme Rat *)
(* liste de df - programme principal - liste de df *)
type programme = Programme of dfs * bloc * dfs

end


(*Module d'affiche des AST issus de la phase d'analyse syntaxique *)
module PrinterAstSyntax : PrinterAst with module A = AstSyntax =
struct

  module A = AstSyntax
  open A

  (* Conversion des opérateurs binaires *)
  let string_of_binaire b =
    match b with
    | Plus -> "+ "
    | Mult -> "* "
    | Equ -> "= "
    | Inf -> "< "
    | Conc -> "^ "   

  (* Conversion des expressions *)
  let rec string_of_expression e =
    match e with
    | AppelFonction (n,le) -> "call "^n^"("^((List.fold_right (fun i tq -> (string_of_expression i)^tq) le ""))^") "
    | Rationnel (e1,e2) -> "["^(string_of_expression e1)^"/"^(string_of_expression e2)^"] "
    | Numerateur e1 -> "num "^(string_of_expression e1)^" "
    | Denominateur e1 ->  "denom "^(string_of_expression e1)^" "
    | True -> "true "
    | False -> "false "
    | Entier i -> (string_of_int i)^" "
    | Binaire (b,e1,e2) -> (string_of_expression e1)^(string_of_binaire b)^(string_of_expression e2)^" "
  	| Chaine (s) -> s
	  | SousChaine(e1, e2, e3) -> "sous-chaine de "^(string_of_expression e1)^"entre "^(string_of_expression e2)^" et "^(string_of_expression e3)^" "
    | Longueur(e) -> "longueur de "^(string_of_expression e)
    | Null -> "Null"
    | Adresse s -> "Adresse de " ^ s
    | New t -> "New de type " ^ (string_of_type t)
    | Acces a -> "Acces à " ^ (string_of_affectable a)

  (* Conversion des affectables *)
  and string_of_affectable a = match a with 
  | Ident n -> n^" "
  | Valeur a -> "Valeur de " ^ (string_of_affectable a)


  (* Conversion des instructions *)
  let rec string_of_instruction i =
    match i with
    | Declaration (t, n, e) -> "Declaration  : "^(string_of_type t)^" "^n^" = "^(string_of_expression e)^"\n"
    | Affectation (a,e) ->  "Affectation  : "^(string_of_affectable a)^" = "^(string_of_expression e)^"\n"
    | Constante (n,i) ->  "Constante  : "^n^" = "^(string_of_int i)^"\n"
    | Affichage e ->  "Affichage  : "^(string_of_expression e)^"\n"
    | Conditionnelle (c,t,e) ->  "Conditionnelle  : IF "^(string_of_expression c)^"\n"^
                                  "THEN \n"^((List.fold_right (fun i tq -> (string_of_instruction i)^tq) t ""))^
                                  "ELSE \n"^((List.fold_right (fun i tq -> (string_of_instruction i)^tq) e ""))^"\n"
    | TantQue (c,b) -> "TantQue  : TQ "^(string_of_expression c)^"\n"^
                                  "FAIRE \n"^((List.fold_right (fun i tq -> (string_of_instruction i)^tq) b ""))^"\n"


  (*Conversion d'un prototype ou d'une fonction*)
  let string_of_df f =
    match f with
    | Prototype(t,n,lp) -> (string_of_type t)^" "^n^" ("^((List.fold_right (fun (t,n) tq -> (string_of_type t)^" "^n^" "^tq) lp ""))^")  \n"
    | Fonction(t,n,lp,li,e) -> (string_of_type t)^" "^n^" ("^((List.fold_right (fun (t,n) tq -> (string_of_type t)^" "^n^" "^tq) lp ""))^") = \n"^
                              ((List.fold_right (fun i tq -> (string_of_instruction i)^tq) li ""))^
                              "Return "^(string_of_expression e)^"\n"
  

  (* Conversion d'un dfs *)
    let string_of_dfs (Dfs lf)= (String.concat " " (List.map string_of_df lf))^"\n"

  (* Conversion d'un programme Rat *)
  let string_of_programme (Programme (dfs1, instruction, dfs2)) =
    string_of_dfs dfs1 ^
    (List.fold_right (fun i tq -> (string_of_instruction i)^tq) instruction "") ^
    string_of_dfs dfs2

  (* Affichage d'un programme Rat *)
  let print_programme programme =
    print_string "AST : \n";
    print_string (string_of_programme programme);
    flush_all ()

end

(*************************************************)
(* AST après la phase d'analyse des identifiants *)
(*************************************************)
module AstTds =
struct

  (* Expressions existantes dans notre langage *)
  (* ~ expression de l'AST syntaxique où les noms des identifiants ont été 
  remplacés par les informations associées aux identificateurs *)
  type expression =
    | AppelFonction of Tds.info_ast list * expression list
    | Rationnel of expression * expression
    | Numerateur of expression
    | Denominateur of expression
    | True
    | False
    | Entier of int
    | Binaire of AstSyntax.binaire * expression * expression
	  | Chaine of string
	  | SousChaine of expression * expression * expression
    | Longueur of expression
    | Null
    | Adresse of Tds.info_ast
    | New of typ
    | Acces of affectable

  (* affectables existants dans notre langage *)
  (* ~ affectables de l'AST syntaxique où les noms des identifiants ont été 
  remplacés par les informations associées aux identificateurs *)
    and affectable = 
    | Ident of Tds.info_ast (* le nom de l'identifiant est remplacé par ses informations *)
    | Valeur of affectable


  (* instructions existantes dans notre langage *)
  (* ~ instruction de l'AST syntaxique où les noms des identifiants ont été 
  remplacés par les informations associées aux identificateurs 
  + suppression de nœuds (const) *)
  type bloc = instruction list
  and instruction =
    | Declaration of typ * expression * Tds.info_ast (* le nom de l'identifiant est remplacé par ses informations *)
    | Affectation of  affectable * expression (* le nom de l'identifiant est remplacé par ses informations *)
    | Affichage of expression
    | Conditionnelle of expression * bloc * bloc
    | TantQue of expression * bloc
    | Empty (* les nœuds ayant disparus: Const *)

  (* Prototype et fonction dans notre langage *)
  (* ~ df de l'AST syntaxique où les noms sont remplacés par les informations associées 
    + suppression des noeuds Prototypes *)
  type df = 
    | Vide (*Les prototypes ne sont plus utilisées , on remplace par vide*)
    | Fonction of typ * Tds.info_ast * (typ * Tds.info_ast ) list * instruction list * expression 

  (* Structure pour les prototypes *)
  type dfs = Dfs of (df list) 

  (* Structure d'un programme dans notre langage *)
  type programme = Programme of dfs * bloc * dfs

end
    

(***********************************)
(* AST après la phase de typage *)
(***********************************)
module AstType =
struct

(* Opérateurs binaires existants dans Rat - résolution de la surcharge *)
type binaire = PlusInt | PlusRat | MultInt | MultRat | EquInt | EquBool | Inf | Conc


(* Affectables existants dans Rat *)
(* = affectable de AstTds + informations associées aux identificateurs, mises à jour  *)
type affectable = 
  | Ident of Tds.info_ast (* le nom de l'identifiant est remplacé par ses informations *)
  | Valeur of affectable

(* Expressions existantes dans Rat *)
(* = expression de AstTds + informations associées aux identificateurs, mises à jour  *)
type expression =
  | AppelFonction of Tds.info_ast * expression list
  | Rationnel of expression * expression
  | Numerateur of expression
  | Denominateur of expression
  | True
  | False
  | Entier of int
  | Binaire of binaire * expression * expression
  | Chaine of string
  | SousChaine of expression * expression * expression
  | Longueur of expression
  | Null
  | Adresse of Tds.info_ast
  | New of typ
  | Acces of affectable



(* instructions existantes Rat *)
(* = instruction de AstTds + informations associées aux identificateurs, mises à jour *)
(* + résolution de la surcharge de l'affichage *)
type bloc = instruction list
 and instruction =
  | Declaration of expression * Tds.info_ast
  | Affectation of affectable * expression 
  | AffichageInt of expression
  | AffichageRat of expression
  | AffichageBool of expression
  | AffichageString of expression
  | Conditionnelle of expression * bloc * bloc
  | TantQue of expression * bloc
  | Empty (* les nœuds ayant disparus: Const *)

(* nom, liste des paramètres, corps, expression de retour, informations associées à l'identificateur *)
type df = Vide
  | Fonction of Tds.info_ast * Tds.info_ast list * instruction list * expression 

(* Structure pour la liste de fonctions et prototypes *)
type dfs = Dfs of (df list) 

(* Structure d'un programme dans notre langage *)
type programme = Programme of dfs * bloc * dfs

let taille_variables_declarees i = 
  match i with
  | Declaration (_,info) -> 
    begin
    match Tds.info_ast_to_info info with
    | InfoVar (_,t,_,_) -> getTaille t
    | _ -> failwith "internal error"
    end
  | _ -> 0 ;;

end

(***********************************)
(* AST après la phase de placement *)
(***********************************)
module AstPlacement =
struct

(* Expressions existantes dans notre langage *)
(* = expression de AstType  *)
type expression = AstType.expression

(* instructions existantes dans notre langage *)
(* = instructions de AstType  *)
type bloc = instruction list
 and instruction = AstType.instruction

(* df existants dans notre langage *)
(* = df de AstType  *)
type df = 
  |Vide (* Ancien prototype*)
  (* nom, corps, expression de retour, informations associées à l'identificateur *)
  (* Plus besoin de la liste des paramètres mais on la garde pour les tests du placement mémoire *)
  |Fonction of Tds.info_ast * Tds.info_ast list * instruction list * expression

(* Structure pour les listes de prototypes et fonctions *)
type dfs = Dfs of (df list)

(* Structure d'un programme dans notre langage *)
type programme = Programme of dfs * bloc * dfs


end


