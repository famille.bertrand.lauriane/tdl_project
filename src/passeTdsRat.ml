(* Module de la passe de gestion des identifiants *)
module PasseTdsRat : Passe.Passe with type t1 = Ast.AstSyntax.programme and type t2 = Ast.AstTds.programme =
struct

  open Tds
  open Exceptions
  open Ast
  open AstTds

  type t1 = Ast.AstSyntax.programme
  type t2 = Ast.AstTds.programme



(* analyse_tds_affectable_ecriture : Tds.tds -> AstSyntax.Affectable -> AstTds.Affectable *)
(* Paramètre tds : la table des symboles courante *)
(* Paramètre a : l'affectable à analyser *)
(* Vérifie la bonne utilisation des identifiants pour un affectable en écriture et tranforme l'affectable
en un affectable de type AstTds.Affectable *)
(* Erreur si mauvaise utilisation des identifiants *)
  let rec analyse_tds_affectable_ecriture tds a = match a with 
  | AstSyntax.Ident (s) -> 
  begin
    match chercherGlobalement tds s with
    | None -> raise (IdentifiantNonDeclare s)
    | Some info -> 
        begin 
          match info_ast_to_info info with
          | InfoFun (s,t,tl,_) -> raise (MauvaiseUtilisationIdentifiant s)
          | InfoConst(s,_) -> raise (MauvaiseUtilisationIdentifiant s)
          | _  -> AstTds.Ident info
        end
  end
  | AstSyntax.Valeur a -> let na = analyse_tds_affectable_ecriture tds a in AstTds.Valeur na 


(* analyse_tds_affectable_lecture : Tds.tds -> AstSyntax.Affectable -> AstTds.Affectable *)
(* Paramètre tds : la table des symboles courante *)
(* Paramètre a : l'affectable à analyser *)
(* Vérifie la bonne utilisation des identifiants pour un affectable en lecture et transforme l'affectable
en un affectable de type AstTds.Affectable *)
(* Erreur si mauvaise utilisation des identifiants *)
  let rec analyse_tds_affectable_lecture tds a = match a with
  | AstSyntax.Ident (s) -> 
  begin
    match chercherGlobalement tds s with
    | None -> raise (IdentifiantNonDeclare s)
    | Some info -> 
        begin 
          match info_ast_to_info info with
          | InfoFun (s,t,tl,_) -> raise (MauvaiseUtilisationIdentifiant s)
          | _  -> AstTds.Ident info
        end
  end
  | AstSyntax.Valeur a -> let na = analyse_tds_affectable_lecture tds a in AstTds.Valeur na


(* analyse_tds_expression : Tds.tds -> AstSyntax.expression -> AstTds.expression *)
(* Paramètre tds : la table des symboles courante *)
(* Paramètre e : l'expression à analyser *)
(* Vérifie la bonne utilisation des identifiants et transforme l'expression
en une expression de type AstTds.expression *)
(* Erreur si mauvaise utilisation des identifiants *)
let rec analyse_tds_expression tds e = 
  match e with
  | AstSyntax.Entier (e) -> AstTds.Entier (e)
  | AstSyntax.Rationnel (e1,e2) -> 
      let a1 = analyse_tds_expression tds e1 in
      let a2 = analyse_tds_expression tds e2 in 
      AstTds.Rationnel (a1, a2)
  | AstSyntax.Numerateur (e) -> 
      let a = analyse_tds_expression tds e in 
      AstTds.Numerateur (a)
  | AstSyntax.Denominateur (e) -> 
      let a = analyse_tds_expression tds e in 
      AstTds.Denominateur (a)
  | AstSyntax.Binaire (b, e1, e2) -> 
      let a1 = analyse_tds_expression tds e1 in
      let a2 = analyse_tds_expression tds e2 in 
      AstTds.Binaire (b,a1, a2)
  | AstSyntax.True -> AstTds.True
  | AstSyntax.False -> AstTds.False
  | AstSyntax.AppelFonction (s, el) ->
      begin
        match chercherGlobalement tds s with
        | None -> raise (IdentifiantNonDeclare s)
        | Some info -> 
            begin 
              match info_ast_to_info info with
              | InfoFun (s,t,tl,_) -> 
                let a = List.map (analyse_tds_expression tds) el in
                (* aux : int -> info_ast list *)
                (* Paramètre : le numéro de la fonction (pour gérer la surcharge) *)
                (* Renvoie la liste des infos_ast des fonctions (surcharge) *)	
                let rec aux indice =
                  match chercherGlobalement tds ((string_of_int indice)^s) with
                  | None -> []
                  | Some info -> info::(aux (indice + 1))
                in AstTds.AppelFonction (info::(aux 1), a)
              | _  -> raise (MauvaiseUtilisationIdentifiant s)
            end
      end
  | AstSyntax.Chaine (s) -> AstTds.Chaine (s)
  | AstSyntax.SousChaine (e1, e2, e3) -> 
		let ne1 = analyse_tds_expression tds e1 in
		let ne2 = analyse_tds_expression tds e2 in
		let ne3 = analyse_tds_expression tds e3 in 
		AstTds.SousChaine (ne1, ne2, ne3)
  | AstSyntax.Longueur (e) -> let ne = analyse_tds_expression tds e in  AstTds.Longueur(ne)
  | AstSyntax.Null -> AstTds.Null
  | AstSyntax.Adresse s ->
     begin
      match chercherGlobalement tds s with
     | None -> raise (IdentifiantNonDeclare s)
      | Some info -> 
        begin 
          match info_ast_to_info info with
          | InfoFun (s,t,tl,_) -> raise (MauvaiseUtilisationIdentifiant s)
          | _  -> AstTds.Adresse info
        end
    end
  | AstSyntax.New t -> AstTds.New t
  | AstSyntax.Acces a -> let na = analyse_tds_affectable_lecture tds a in AstTds.Acces(na)


  

(* analyse_tds_instruction : Tds.tds -> AstSyntax.instruction -> AstTds.instruction *)
(* Paramètre tds : la table des symboles courante *)
(* Paramètre i : l'instruction à analyser *)
(* Vérifie la bonne utilisation des identifiants et transforme l'instruction
en une instruction de type AstTds.instruction *)
(* Erreur si mauvaise utilisation des identifiants *)
let rec analyse_tds_instruction tds i =
  match i with
  | AstSyntax.Declaration (t, n, e) ->
      begin
        match chercherLocalement tds n with
        | None ->
            (* L'identifiant n'est pas trouvé dans la tds locale, 
            il n'a donc pas été déclaré dans le bloc courant *)
            (* Vérification de la bonne utilisation des identifiants dans l'expression *)
            (* et obtention de l'expression transformée *) 
            let ne = analyse_tds_expression tds e in
            (* Création de l'information associée à l'identifiant *)
            let info = InfoVar (n,Undefined, 0, "") in
            (* Création du pointeur sur l'information *)
            let ia = info_to_info_ast info in
            (* Ajout de l'information (pointeur) dans la tds *)
            ajouter tds n ia;
            (* Renvoie de la nouvelle déclaration où le nom a été remplacé par l'information 
            et l'expression remplacée par l'expression issue de l'analyse *)
            AstTds.Declaration (t, ne, ia) 
        | Some _ ->
            (* L'identifiant est trouvé dans la tds locale, 
            il a donc déjà été déclaré dans le bloc courant *) 
            raise (DoubleDeclaration n)
      end
  | AstSyntax.Affectation (a,e) ->
      let na = analyse_tds_affectable_ecriture tds a in
      let ne = analyse_tds_expression tds e in
      AstTds.Affectation(na,ne)
  | AstSyntax.Constante (n,v) -> 
      begin
        match chercherLocalement tds n with
        | None -> 
        (* L'identifiant n'est pas trouvé dans la tds locale, 
        il n'a donc pas été déclaré dans le bloc courant *)
        (* Ajout dans la tds de la constante *)
        ajouter tds n (info_to_info_ast (InfoConst (n,v))); 
        (* Suppression du noeud de déclaration des constantes, devenu inutile *)
        Empty
        | Some _ ->
          (* L'identifiant est trouvé dans la tds locale, 
          il a donc déjà été déclaré dans le bloc courant *) 
          raise (DoubleDeclaration n)
      end
  | AstSyntax.Affichage e -> 
      (* Vérification de la bonne utilisation des identifiants dans l'expression *)
      (* et obtention de l'expression transformée *)
      let ne = analyse_tds_expression tds e in
      (* Renvoie du nouvel affichage où l'expression remplacée par l'expression issue de l'analyse *)
      Affichage (ne)
  | AstSyntax.Conditionnelle (c,t,e) -> 
      (* Analyse de la condition *)
      let nc = analyse_tds_expression tds c in
      (* Analyse du bloc then *)
      let tast = analyse_tds_bloc tds t in
      (* Analyse du bloc else *)
      let east = analyse_tds_bloc tds e in
      (* Renvoie la nouvelle structure de la conditionnelle *)
      Conditionnelle (nc, tast, east)
  | AstSyntax.TantQue (c,b) -> 
      (* Analyse de la condition *)
      let nc = analyse_tds_expression tds c in
      (* Analyse du bloc *)
      let bast = analyse_tds_bloc tds b in
      (* Renvoie la nouvelle structure de la boucle *)
      TantQue (nc, bast)





(* analyse_tds_bloc : Tds.tds -> AstSyntax.bloc -> AstTds.bloc *)
(* Paramètre tds : la table des symboles courante *)
(* Paramètre li : liste d'instructions à analyser *)
(* Vérifie la bonne utilisation des identifiants et transforme le bloc
en un bloc de type AstTds.bloc *)
(* Erreur si mauvaise utilisation des identifiants *)
and analyse_tds_bloc tds li =
  (* Entrée dans un nouveau bloc, donc création d'une nouvelle tds locale 
  pointant sur la table du bloc parent *)
  let tdsBloc = creerTDSFille tds in
  (* Analyse des instructions du bloc avec la tds du nouveau bloc 
  Cette tds est modifiée par effet de bord *)
  List.map (analyse_tds_instruction tdsBloc) li

(* ajout_param : Tds.tds -> typ * string -> typ * info_ast *)
(* Paramètre x : le type du parametre *)
(* Paremètre y : le nom du parametre *)
(* Ajoute le paramètre à la Tds et retourne le type et l'info. *)	
(* Erreur si mauvaise utilisation *)
let ajout_param tdsParam (x,y) = 
  match chercherLocalement tdsParam y with
  | None -> let infoast = info_to_info_ast(InfoVar(y,x,0,"adresse")) in  
            ajouter tdsParam y infoast; 
            (x, infoast)
  | Some _ -> raise (DoubleDeclaration y)


(* analyse_tds_prototype : Tds.tds -> AstSyntax.df -> AstTds.tds *)
(* Paramètre tds : la table des symboles courante *)
(* Paramètre li : liste d'instructions à analyser *)
(* Vérifie la bonne utilisation des identifiants, crée l'info et 
renvoie un AstTds.df *)
(* Erreur si mauvaise utilisation des identifiants *)
let analyse_tds_prototype maintds df =
  match df with
  | AstSyntax.Prototype(t,n,lp) -> 
    (* aux : int -> AstTds.df *)
    (* Paramètre : le numéro du prototype (pour gérer la surcharge) *)
    (* Renvoie un AstTds.df *)	
    let rec aux indice =
      let nom = if (indice == 0) then n else (string_of_int indice)^n in
      begin
        match chercherGlobalement maintds nom with
        | None ->
          (* L'identifiant n'est pas trouvé dans la tds locale, 
          il n'a donc pas été déclaré dans le bloc courant *)

          (* Création de l'information associée à l'identifiant *)
          let info = InfoFun (nom, t, List.map fst lp, false) in
          (* Création du pointeur sur l'information *)
          let ia = info_to_info_ast info in
          (* Ajout de l'information (pointeur) dans la tds *)
          ajouter maintds nom ia;
          (* Ajouter les param dans la tds et transformation en info_ast*)
          let tdsParam = creerTDSFille maintds in
          let _ = List.map (ajout_param tdsParam) lp in

          AstTds.Vide
        | Some i -> 
          begin
            match info_ast_to_info i with
            | InfoFun (_, typ, lp2, _) -> 
              if (Type.est_compatible t typ) then
                if (Type.est_compatible_list (List.map fst lp) lp2) then AstTds.Vide else aux (indice+1)
              else raise (DoubleDeclaration nom)
            | _ -> raise (DoubleDeclaration nom)
          end
      end
    in aux 0
  | _ -> raise (Failure "erreur interne")


(* analyse_tds_fonction : Tds.tds -> AstSyntax.df -> AstTds.df *)
(* Paramètre tds : la table des symboles courante *)
(* Paramètre f : la fonction à analyser *)
(* Vérifie la bonne utilisation des identifiants et tranforme la fonction
en une fonction de type AstTds.df *)
(* Erreur si mauvaise utilisation des identifiants *)
let analyse_tds_fonction maintds f  =
  match f with
  | AstSyntax.Fonction(t,n,lp,li,e) ->
    (* aux : int -> AstTds.df *)
    (* Paramètre : le numéro de la fonction (pour gérer la surcharge) *)
    (* Renvoie un AstTds.df *)	
    let rec aux indice =
      let nom = if (indice == 0) then n else (string_of_int indice)^n in
      begin
        match chercherGlobalement maintds nom with
        | None ->
          (* L'identifiant n'est pas trouvé dans la tds locale, 
          il n'a donc pas été déclaré dans le bloc courant *)
          
          (* Création de l'information associée à l'identifiant *)
          let info = InfoFun (nom, t, List.map fst lp, true) in
          (* Création du pointeur sur l'information *)
          let ia = info_to_info_ast info in
          (* Ajout de l'information (pointeur) dans la tds *)
          ajouter maintds nom ia;
          (* Ajouter les parmam dans la tds et transformation en info_ast*)
          let tdsParam = creerTDSFille maintds in
          let nlp = List.map (ajout_param tdsParam) lp in

          (* Vérification de la bonne utilisation des identifiants dans les instructions *)
          let tdsInstr = creerTDSFille tdsParam in 
          let nli = List.map (analyse_tds_instruction tdsInstr) li in

          (* Vérification de la bonne utilisation des identifiants dans l'expression *)
          (* et obtention de l'expression transformée *) 
          let ne = analyse_tds_expression tdsInstr e in
          AstTds.Fonction (t, ia, nlp, nli, ne)
        | Some i -> 
          begin
            match info_ast_to_info i with
            | InfoFun (_, typ, lp2, true) -> 
              if (Type.est_compatible t typ) then
                if (Type.est_compatible_list (List.map fst lp) lp2) then raise (DoubleDeclaration nom) else aux (indice+1)
              else raise (DoubleDeclaration nom)
            | InfoFun (_, typ, lp2, false) -> 
              if (Type.est_compatible t typ) then
                if (Type.est_compatible_list (List.map fst lp) lp2) then 
                  (modifier_bool_fonction_info true i;

                  (* Ajouter les parmam dans la tds et transformation en info_ast*)
                  let tdsParam = creerTDSFille maintds in
                  let nlp = List.map (ajout_param tdsParam) lp in

                  (* Vérification de la bonne utilisation des identifiants dans les instructions *)
                  let tdsInstr = creerTDSFille tdsParam in 
                  let nli = List.map (analyse_tds_instruction tdsInstr) li in
        
                  (* Vérification de la bonne utilisation des identifiants dans l'expression *)
                  (* et obtention de l'expression transformée *) 
                  let ne = analyse_tds_expression tdsInstr e in
                  AstTds.Fonction (t, i, nlp, nli, ne))
                else aux (indice+1)
              else raise (DoubleDeclaration n)
            | _ -> raise (DoubleDeclaration nom)
          end            
      end
    in aux 0
  | _ -> raise (Failure "erreur interne")
  



(* analyse_tds_dfs : Tds.tds -> AstSyntax.dfs -> AstTds.dfs *)
(* Paramètre tds : la table des symboles courante  *)
(* Paramètre ldfs : la liste des prototypes et fonctions à analyser *)
(* Vérifie la bonne utilisation des identifiants et 
renvoie un élément de type AstTds.ast *)
(* Erreur si mauvaise utilisation des identifiants *)
let analyse_tds_dfs tds (AstSyntax.Dfs(ldfs)) =
  let aux f = 
    match f with
    | AstSyntax.Prototype(_,_,_) -> (analyse_tds_prototype tds f)
    | AstSyntax.Fonction(_,_,_,_,_) -> (analyse_tds_fonction tds f)
  in
  let nlf = List.map aux ldfs in 
  AstTds.Dfs(nlf)

(* analyser : AstSyntax.programme -> AstTds.programme *)
(* Paramètre : le programme à analyser *)
(* Vérifie la bonne utilisation des identifiants et transforme le programme
en un programme de type AstTds.programme *)
(* Erreur si mauvaise utilisation des identifiants *)
let analyser (AstSyntax.Programme (dfs1, b, dfs2)) =
  let mainTDS = Tds.creerTDSMere() in
  let ndfs1 = analyse_tds_dfs mainTDS dfs1 in
  let nb = analyse_tds_bloc mainTDS b in
  let ndfs2 = analyse_tds_dfs mainTDS dfs2 in
  
  (* Vérifie que tous les prototypes ont été implantés *)
  if (prototypes_redefinis mainTDS) then AstTds.Programme(ndfs1, nb, ndfs2)
  else raise PrototypePasImplante
  
end
