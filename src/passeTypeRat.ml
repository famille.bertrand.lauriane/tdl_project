(* Module de la passe de vérification du typage *)
module PasseTypeRat : Passe.Passe with type t1 = Ast.AstTds.programme and type t2 = Ast.AstType.programme =
struct

  open Tds
  open Type
  open Exceptions
  open Ast
  open AstTds
  open AstType

  type t1 = Ast.AstTds.programme
  type t2 = Ast.AstType.programme


(* analyse_type_affectable : AstTds.Affectable-> typ * AstType.Affectable *)
(* Paramètre a : l'affectable à analyser *)
(* Détermine le type des affectables, le renvoie et transforme l'affectable
en un affectable de type AstType.Affectable *)
let rec analyse_type_affectable a = match a with
| AstTds.Ident(s) -> 
begin
    match info_ast_to_info s with
    | InfoVar (_, t, _, _) -> (t, AstType.Ident s)
    | InfoConst (_, _) -> (Int, AstType.Ident s)
    | _ -> raise (Failure "Erreur interne")
end
| AstTds.Valeur a-> let (t,na) = analyse_type_affectable a in 
    begin 
        match t with 
        | Pointeur t2 -> (t2,AstType.Valeur na)
        | _-> raise (PasUnPointeur t)
    end


(* analyse_type_expression : AstTds.expression -> typ * AstType.expression *)
(* Paramètre e : l'expression à analyser *)
(* Détermine le type des expressions, le renvoie et transforme l'expression
en une expression de type AstType.expression *)
let rec analyse_type_expression e = 
    match e with
    | AstTds.Entier (e) -> (Int, AstType.Entier e)
    | AstTds.Rationnel (e1, e2) -> 
        let (t1, ne1) = analyse_type_expression e1 in
        let (t2, ne2) = analyse_type_expression e2 in 
        if (est_compatible t1 Int) then
            if (est_compatible t2 Int) then (Rat, AstType.Rationnel (ne1, ne2))
            else raise (TypeInattendu (t2, Int))
        else raise (TypeInattendu (t1, Int))
    | AstTds.Numerateur (e1) -> 
        let (t1, ne1) = analyse_type_expression e1 in
        if (est_compatible t1 Rat) then (Int, AstType.Numerateur ne1)
        else raise (TypeInattendu (t1, Rat))
    | AstTds.Denominateur (e1) -> 
        let (t1, ne1) = analyse_type_expression e1 in 
        if (est_compatible t1 Rat) then (Int, AstType.Denominateur ne1)
        else raise (TypeInattendu (t1, Rat))
    | AstTds.Binaire (op, e1, e2) -> 
        let (t1, ne1) = analyse_type_expression e1 in
        let (t2, ne2)= analyse_type_expression e2 in 
        begin
        match (op, t1, t2) with
        | (Plus, Int, Int) ->(Int, AstType.Binaire (PlusInt, ne1, ne2))
        | (Plus, Rat, Rat) -> (Rat, AstType.Binaire (PlusRat, ne1, ne2))
        | (Plus, type1, type2) -> raise (TypeBinaireInattendu (Plus, type1, type2))
        | (Mult, Int, Int) -> (Int, AstType.Binaire (MultInt, ne1, ne2))
        | (Mult, Rat, Rat) -> (Rat, AstType.Binaire (MultRat, ne1, ne2))
        | (Mult, type1, type2) -> raise (TypeBinaireInattendu (Mult, type1, type2))
        | (Equ, Int, Int) -> (Bool, AstType.Binaire (EquInt, ne1, ne2))
        | (Equ, Bool, Bool) -> (Bool, AstType.Binaire (EquBool, ne1, ne2))
        | (Equ, type1, type2) -> raise (TypeBinaireInattendu (Equ, type1, type2))
        | (Inf, Int, Int) -> (Bool, AstType.Binaire (Inf, ne1, ne2))
        | (Inf, type1, type2) -> raise (TypeBinaireInattendu (Inf, type1, type2)) 
        | (Conc, String, String) -> (String, AstType.Binaire (Conc, ne1, ne2))
        | (Conc, type1, type2) -> raise (TypeBinaireInattendu (Conc, type1, type2)) 
        end
    | AstTds.True -> (Bool, AstType.True)
    | AstTds.False -> (Bool, AstType.False)
    | AstTds.AppelFonction (li, le) ->
        let (lte, nle) = List.split (List.map analyse_type_expression le) in
        (* aux : info_ast list -> typ list list ->  typ * AstType.expression *)
        (* Paramètre li : liste des infos de l'appel de fonction *)
        (* Paramètre tparam : Liste des paramètres des fonctions appelées (surcharge) *)
        (* Renvoie le type retour et la transformation en AstType.AppelFonction *)	
        let rec aux li tparam = 
            match li with
            | [] -> raise (TypesParametresInattendus (lte, tparam))
            | t::q -> 
                begin
                    match info_ast_to_info t with
                    | InfoFun (nom, typeret, typeparam, _) ->
                        if (est_compatible_list typeparam lte) then (typeret, AstType.AppelFonction(t, nle))
                        else aux q (typeparam::tparam)
                    | _ -> raise (Failure "erreur interne")
                end
        in aux li []
	| AstTds.Chaine (s)-> (String, AstType.Chaine s)
    | AstTds.SousChaine(e1, e2, e3)->  
		let (t1, ne1) = analyse_type_expression e1 in
		let (t2, ne2) = analyse_type_expression e2 in
		let (t3, ne3) = analyse_type_expression e3 in		
        if (est_compatible t1 String) then 
			(if (est_compatible t2 Int) then 
				(if (est_compatible t3 Int) 
					then (String, AstType.SousChaine(ne1,ne2,ne3))
					else raise (TypeInattendu (t3, Int)))
			else raise (TypeInattendu (t2, Int)))
		else raise (TypeInattendu (t1, String))
    | AstTds.Longueur (e1) ->  
		let (t1, ne1) = analyse_type_expression e1 in
        if (est_compatible t1 String) then (Int, AstType.Longueur ne1)
        else raise (TypeInattendu (t1, String))
    
    | AstTds.Null -> (Pointeur(Undefined), AstType.Null)
    | AstTds.Adresse s -> 
        begin
            match info_ast_to_info s with
            | InfoVar (_, t, _, _) -> (Pointeur t, AstType.Adresse s)
            | InfoConst (_, _) -> (Pointeur Int, AstType.Adresse s)
            | _ -> raise (Failure "Erreur interne")
        end
  | AstTds.New t -> (Pointeur t, AstType.New t)
  | AstTds.Acces a -> let (t,na) = analyse_type_affectable a in (t,AstType.Acces(na))




(* analyse_type_instruction : AstTds.instruction -> AstType.instruction *)
(* Paramètre i : l'instruction à analyser *)
(* Vérifie le bon typage des instructions et transforme l'instruction
en une instruction de type AstType.instruction *)
(* Erreur si mauvais typage *)
let rec analyse_type_instruction i =
    match i with
    | AstTds.Declaration (t, expr, info_ast) ->
        let (te, ne) = analyse_type_expression expr in
        if (est_compatible t te) then 
	    begin
                modifier_type_info t info_ast;
                AstType.Declaration(ne, info_ast)
	    end
        else raise (TypeInattendu (te, t))
    | AstTds.Affectation (a,e) ->
        let (ta, na) = analyse_type_affectable a in
        let (te, ne) = analyse_type_expression e in
            if (est_compatible ta te) then AstType.Affectation(na,ne)
            else raise (TypeInattendu (te, ta))   
    | AstTds.Affichage e -> 
        let (te, ne) = analyse_type_expression e in
        begin
            match te with
            | Int -> AstType.AffichageInt ne
            | Bool -> AstType.AffichageBool ne
            | Rat -> AstType.AffichageRat ne
            | String -> AstType.AffichageString ne
            | Pointeur _ -> raise (Failure "pas d'affichage pointeur")
			| _ -> raise (Failure "erreur interne")
        end
    | AstTds.Conditionnelle (e, b1, b2) -> 
        let (te, ne) = analyse_type_expression e in
        if (est_compatible te Bool) then 
            let nb1 = analyse_type_bloc b1 in
            let nb2 = analyse_type_bloc b2 in
            AstType.Conditionnelle(ne, nb1, nb2)
        else
            raise (TypeInattendu (te, Bool))
    | AstTds.TantQue (e, b) -> 
        let (te, ne) = analyse_type_expression e in
        if (est_compatible te Bool) then 
            let nb = analyse_type_bloc b in
            AstType.TantQue(ne, nb)
        else
            raise (TypeInattendu (te, Bool))
    | AstTds.Empty -> AstType.Empty

      
(* analyse_type_bloc : AstTds.bloc -> AstType.bloc *)
(* Paramètre li : liste d'instructions à analyser *)
(* Vérifie le bon typage des instructions du bloc et transforme le bloc
en un bloc de type AstType.bloc *)
(* Erreur si mauvais typage *)
and analyse_type_bloc li =
  (* Analyse des instructions du bloc *)
  List.map analyse_type_instruction li

(* analyse_type_df : AstTds.df -> AstType.df *)
(* Paramètre : le df à analyser *)
(* Vérifie le bon typage des fonctions et transforme la fonction
en une fonction de type AstType.fonctions *)
(* Erreur si mauvais typage *)
let analyse_type_df df  =
    match df with
    | AstTds.Fonction(tr, i, lparam, li, e) -> 
        modifier_type_fonction_info tr (List.map fst lparam) i;
        let _ = List.map (fun (x,y) -> modifier_type_info x y) lparam in
        let nli = List.map analyse_type_instruction li in
        let (te, ne) = analyse_type_expression e in
        if (est_compatible te tr) then AstType.Fonction(i, List.map snd lparam, nli, ne)
        else raise (TypeInattendu (te, tr))
    | _ -> AstType.Vide

(* analyse_type_dfs : AstTds.dfs -> AstType.dfs *)
(* Paramètre : la liste de dfà analyser *)
(* Vérifie le bon typage des fonctions et transforme la liste de fonctions
en une liste de fonctions de type AstType.dfs *)
(* Erreur si mauvais typage *)
let analyse_type_dfs (AstTds.Dfs ldfs)  =
    let nlf = List.map analyse_type_df ldfs in 
    AstType.Dfs(nlf)

(* analyser : AstTds.programme -> AstType.programme *)
(* Paramètre : le programme à analyser *)
(* Vérifie le bon typage du programme et transforme le programme
en un programme de type AstType.programme *)
(* Erreur si mauvais typage *)
let analyser (AstTds.Programme (dfs1,b, dfs2)) =
    let tdfs1 = analyse_type_dfs dfs1 in
    let tb = analyse_type_bloc b in 
    let tdfs2 = analyse_type_dfs dfs2 in
    AstType.Programme(tdfs1, tb, tdfs2)

end
