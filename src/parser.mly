/* Imports. */

%{

open Type
open Ast.AstSyntax
%}


%token <int> ENTIER
%token <string> ID
%token RETURN
%token PV
%token AO
%token AF
%token PF
%token PO
%token EQUAL
%token CONST
%token PRINT
%token IF
%token ELSE
%token WHILE
%token BOOL
%token INT
%token RAT
%token CALL 
%token CO
%token CF
%token SLASH
%token NUM
%token DENOM
%token TRUE
%token FALSE
%token PLUS
%token MULT
%token INF
%token EOF

%token CONC
%token VIRG
%token LENGTH
%token STRING
%token <string> CHAINE

%token NEW
%token NULL
%token ET

(* Type de l'attribut synthétisé des non-terminaux *)
%type <programme> prog
%type <instruction list> bloc
%type <df> fonc
%type <instruction list> is
%type <instruction> i
%type <typ> typ
%type <(typ*string) list> dp
%type <expression> e 
%type <expression list> cp

(* Type et définition de l'axiome *)
%start <Ast.AstSyntax.programme> main

%%

main : lfi = prog EOF     {lfi}

prog :
| d1 =dfs b=bloccp d2 =dfs { Programme(d1,b,d2)}

(*
| lf = fonc  lfi = prog   {let (Programme (lf1,li))=lfi in (Programme (lf::lf1,li))}
| ID li = bloc            {Programme ([],li)}
*)

dfs :
|                         {Dfs([])}
| d= decl lf=dfs           { let Dfs lf1 = lf in (Dfs (d::lf1))}
| f = fonc lf =dfs           { let Dfs lf1 = lf in (Dfs (f::lf1))}

decl : t=typ n=ID PO p=dp PF PV        {Prototype(t,n,p)}

fonc : t=typ n=ID PO p=dp PF AO li=is RETURN exp=e PV AF {Fonction(t,n,p,li,exp)}

bloccp : ID b = bloc      {b}

bloc : AO li = is AF      {li}

is :
|                         {[]}
| i1=i li=is              {i1::li}

i :
| t=typ n=ID EQUAL e1=e PV          {Declaration (t,n,e1)}
| a1=a EQUAL e1=e PV                {Affectation (a1,e1)}
| CONST n=ID EQUAL e=ENTIER PV      {Constante (n,e)}
| PRINT e1=e PV                     {Affichage (e1)}
| IF exp=e li1=bloc ELSE li2=bloc   {Conditionnelle (exp,li1,li2)}
| WHILE exp=e li=bloc               {TantQue (exp,li)}

a :
| n=ID                    {Ident n}
| PO MULT a1=a PF         {Valeur a1}

dp :
|                         {[]}
| t=typ n=ID lp=dp        {(t,n)::lp}

typ :
| BOOL    {Bool}
| INT     {Int}
| RAT     {Rat}
| t=typ MULT {Pointeur t}
| STRING    {String}

e : 
| CALL n=ID PO lp=cp PF   {AppelFonction (n,lp)}
| CO e1=e SLASH e2=e CF   {Rationnel(e1,e2)}
| NUM e1=e                {Numerateur e1}
| DENOM e1=e              {Denominateur e1}
| TRUE                    {True}
| FALSE                   {False}
| e=ENTIER                {Entier e}
| PO e1=e PLUS e2=e PF    {Binaire (Plus,e1,e2)}
| PO e1=e MULT e2=e PF    {Binaire (Mult,e1,e2)}
| PO e1=e EQUAL e2=e PF   {Binaire (Equ,e1,e2)}
| PO e1=e INF e2=e PF     {Binaire (Inf,e1,e2)}
| a1=a                    {Acces a1}
| NULL                    {Null}
| PO NEW t=typ PF         {New t}
| ET n=ID                 {Adresse n}
| s=CHAINE   			  {Chaine s}
| PO e1=e CONC e2=e PF    {Binaire(Conc,e1,e2)}
| PO e1=e AO e2 =e VIRG e3=e AF PF    {SousChaine(e1,e2,e3)}
| LENGTH e1=e		      {Longueur e1}



cp :
|               {[]}
| e1=e le=cp    {e1::le}

