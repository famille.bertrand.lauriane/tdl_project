(* Module de la passe de génération de code *)

module PasseCodeRatToTam : Passe.Passe with type t1 = Ast.AstPlacement.programme and type t2 = string =
struct

  open Tds
  open Type
  open Code
  open Exceptions
  open Ast
  open AstType
  open AstPlacement

  type t1 = Ast.AstPlacement.programme
  type t2 = string


(* aux_ecriture_valeur : AstType.Affectable -> int * string * string *)
(* Paramètre : l'affectable à analyser *)
(* Renvoie l'emplacement, le registre et le code TAM de l'affectable en écriture *)	
let rec aux_ecriture_valeur a = match a with 
	| AstType.Ident info -> 
		begin
		match info_ast_to_info info with
			| InfoVar (_, typ, dep ,reg) -> dep, reg,("STOREI (" ^ (string_of_int (getTaillePointeur typ)) ^ ") \n")
			| _ -> raise (Failure "erreur interne")
		end
	| AstType.Valeur a -> let dep, reg, code = aux_ecriture_valeur a in dep, reg, ("LOADI(1) \n"^code) 

(* analyse_code_affectable_ecriture : AstPlacement.Affectable -> string *)
(* Paramètre : l'affectable à analyser *)
(* Génère le code Tam de l'affectable en écriture *)	
let rec analyse_code_affectable_ecriture a = match a with
	| AstType.Ident (info) -> 
		begin
		match info_ast_to_info info with
		| InfoVar (n, typ, dep, reg) -> 
			"STORE ("^(string_of_int (getTaille typ))^") " ^(string_of_int dep)^"["^reg^"] \n" 
		| _ -> raise (Failure "erreur interne")
		end
	| AstType.Valeur a -> let (dep, reg, code) = (aux_ecriture_valeur a) in "LOAD (1) " ^(string_of_int dep)^" ["^reg^"] \n" ^code


		

(* aux_lecture_valeur : AstType.Affectable -> int * string *)
(* Paramètre : l'affectable à analyser *)
(* Renvoie la taille et le code TAM de l'affectable en lecture *)	
let rec aux_lecture_valeur a = match a with 
	| AstType.Ident info -> 
		begin
		match info_ast_to_info info with
		| InfoVar (_, typ, _ , _) -> getTaillePointeur typ, analyse_code_affectable_lecture a
		| InfoConst (n, v) -> 1, analyse_code_affectable_lecture a
		| _ -> raise (Failure "erreur interne")
		end
	| AstType.Valeur a -> let taille, code = (aux_lecture_valeur a) in taille, code^"LOADI (1) \n"  (* Dans ce cas a est un pointeur donc sa taille est 1*)

(* analyse_code_affectable_lecture : AstPlacement.Affectable -> string *)
(* Paramètre : l'affectable à analyser *)
(* Gènère le code Tam de l'affectable en lecture *)	
and analyse_code_affectable_lecture a = 
	match a with
	| AstType.Ident (info) -> 
		begin
		match info_ast_to_info info with
		| InfoVar (n, typ, dep, reg) -> "LOAD ("^(string_of_int (getTaillePointeur typ))^") " ^(string_of_int dep)^" ["^reg^"] \n"
		| InfoConst (n, v) -> "LOADL "^(string_of_int v)^" \n"
		| _ -> raise (Failure "erreur interne")
		end
	| AstType.Valeur a -> let taille, code = aux_lecture_valeur a in code^"LOADI (" ^ (string_of_int (taille)) ^ ") \n"



(* analyse_code_expression : AstPlacement.expression -> string *)
(* Paramètre : l'expression à analyser *)
(* Génère le code Tam de l'expression *)
let rec analyse_code_expression e =
    match e with 
    | AstType.AppelFonction (info, le) -> 
        begin
            match info_ast_to_info info with
            | InfoFun (n, _, _, _) -> (String.concat " " (List.map analyse_code_expression le))^"CALL (SB) "^n^" \n"
            | _ -> raise (Failure "erreur interne")
        end
    | AstType.Binaire (op, e1, e2) ->
        let ce1 = analyse_code_expression e1 in
        let ce2 = analyse_code_expression e2 in
        begin  
            match op with
            | PlusInt -> ce1^ce2^"SUBR IAdd \n"
            | PlusRat -> ce1^ce2^"CALL (SB) radd \n"
            | MultInt -> ce1^ce2^"SUBR IMul \n"
            | MultRat -> ce1^ce2^"CALL (SB) rmul \n"
            | EquInt -> ce1^ce2^"SUBR IEq \n"
            | EquBool -> ce1^ce2^"SUBR IEq \n"
            | Inf -> ce1^ce2^"SUBR ILss \n"
            | Conc -> ce1^ce2^"CALL (SB) scat \n"
        end
    | AstType.Rationnel (e1, e2) -> 
        let ce1 = analyse_code_expression e1 in 
        let ce2 = analyse_code_expression e2 in 
        ce1^ce2
    | AstType.Numerateur (e) -> 
        let ce = analyse_code_expression e in 
        ce^"POP (0) 1 \n"
    | AstType.Denominateur (e) -> 
        let ce = analyse_code_expression e in 
        ce^"POP (1) 1 \n"
    | AstType.True -> "LOADL 1 \n"
    | AstType.False -> "LOADL 0 \n"
    | AstType.Entier (v) -> "LOADL "^(string_of_int v)^"\n"
    | AstType.Chaine (s)-> 
		begin
        (* ecrire_chaine : string -> int -> string *)
        (* Paramètre s : la chaine a ecrire *)
        (* Paramètre l : la longueur de la chaine *)
        (* Génère le code Tam pour écrire la chaine *)
		let rec ecrire_chaine s l = 
            match l with
			| 1 -> ""
			|_-> (ecrire_chaine s (l-1)) ^ "LOADL '" ^ (Char.escaped s.[l-1]) ^ "' \n "
		in 
		let l = (String.length s)-2 in (* On enlève deux pour enlever les guillemets *)
        (* Allouer un espace de taille 1 + taille chaine, on ajoute un car il faut aussi stocker la taille de la chaine *)
		"LOADL "^ (string_of_int (l+1)) ^"\n"
		^"SUBR MAlloc  \n"
		(* On écrit la taille dans la pile *)
		^"LOADL "^ (string_of_int l) ^"\n"
		(* On écrit la chaine dans la pile *)
		^(ecrire_chaine s ((String.length s) -1))
		(* On copie l'adresse où la chaine doit être stockée *)
		^"LOAD (1) -"^(string_of_int (l+2))^"[ST] \n"
		(* On place la taille de la chaine, suivie de la chaine, à son adresse *)
		^"STOREI ("^(string_of_int (l+1))^") \n"
		end
    | AstType.SousChaine (e1, e2 ,e3)-> 
        let ce1 = analyse_code_expression e1 in
        let ce2 = analyse_code_expression e2 in
        let ce3 = analyse_code_expression e3 in
        ce1^ce2^ce3^"CALL (SB) ssub \n"
    | AstType.Longueur (e)-> 
		let ce = analyse_code_expression e in
        ce^"LOADI (1) \n"
    | AstType.Null -> "LOADL (1) \n"^"SUBR MAlloc"
    | AstType.Adresse info -> 
		begin
		match info_ast_to_info info with
		| InfoVar (_, typ, dep , reg) -> "LOADA " ^ (string_of_int dep) ^ "["^reg^"] \n"
		| _ -> raise (Failure "erreur interne")
		end
    | AstType.New t ->
        (* code_new : typ -> string *)
        (* Paramètres : le type pointé  *)
        (* Génère le code Tam pour écrire le pointeur *)
		let rec code_new t  = 
            (match t with 
			| Pointeur(t1) -> "LOADL "^(string_of_int (getTaille t1))
                ^"\n SUBR MAlloc \n LOAD (1) -1 [ST] \n LOAD (1) -3 [ST] \n STOREI (1) \n"
                ^ (code_new t1)
			| _ -> "")
		in "LOADL " ^ (string_of_int (getTaille t)) ^ "\n" ^ "SUBR MAlloc \n" ^ (code_new t)
		
    | AstType.Acces a -> let ca = analyse_code_affectable_lecture a in ca
    
        

(* analyse_code_instruction : AstType.placement -> string *)
(* Paramètre : l'instruction à analyser *)
(* Génère le code Tam d'une instrcution *)
let rec analyse_code_instruction i = 
    match i with
    | Declaration (e, info) -> 
        begin  
            match info_ast_to_info info with
            | InfoVar (_, t,  dep, reg) -> 
				"PUSH "^(string_of_int (getTaille t))^" \n"^
                (analyse_code_expression e)^
                "STORE ("^(string_of_int (getTaille t))^") "^(string_of_int dep)^" ["^reg^"] \n"
            | _ -> raise (Failure "erreur interne")
        end
    | TantQue (e, b) ->
        let ldeb = getEtiquette() in 
        let lfin = getEtiquette() in 
        let ce = analyse_code_expression e in 
        let cb = analyse_code_bloc b in 
        ldeb^" \n"^ce^"JUMPIF (0) "^lfin^" \n"^cb^"JUMP "^ldeb^" \n"^lfin^" \n"
    | AffichageInt e -> (analyse_code_expression e)^"SUBR IOut \n"
    | AffichageBool e -> (analyse_code_expression e)^"SUBR BOut \n"
    | AffichageRat e -> (analyse_code_expression e)^"CALL (SB) rout \n"
    | AffichageString e -> (analyse_code_expression e)^"CALL (SB) sout \n"
    | Affectation (a, e) -> 
		let ca = analyse_code_affectable_ecriture a in
        let ce = analyse_code_expression e in 
        ce^ca
    | Conditionnelle (e, b1, b2) -> 
        let lsinon = getEtiquette() in 
        let lfin = getEtiquette() in 
        let ce = analyse_code_expression e in 
        let cb1 = analyse_code_bloc b1 in 
        let cb2 = analyse_code_bloc b2 in 
        ce^"JUMPIF (0) "^lsinon^" \n"^cb1^"JUMP "^lfin^" \n"^lsinon^" \n"^cb2^lfin^" \n"
    | Empty -> ""

      
(* analyse_code_bloc : AstPlacement.Bloc -> string *)
(* Paramètre : le bloc à analyser *)
(* Génère le code Tam d'un bloc *)
and analyse_code_bloc li = 
    let aux i tq = match i with
        | Declaration (_, info_ast) -> 
            let info = info_ast_to_info info_ast in
            begin  
                match info with
                | InfoVar (_, t, _, _) -> (getTaille t) + tq
                | InfoConst (_,_) -> tq
                | _ -> raise (Failure "erreur interne")
            end
        | _ -> tq
    in let tailleLocale = List.fold_right aux li 0 in 
    (String.concat " " (List.map analyse_code_instruction li))^"POP (0) "^(string_of_int tailleLocale)^" \n"



(* analyse_code_param : infos_ast list -> string *)
(* Paramètre : la liste de paramètres d'une fonction à analyser *)
(* Génère le code Tam de la liste de paramètres *)
let analyse_code_param lparam =
    (* aux: infos_ast -> string -> string *)
    (* Paramètre infoast : un paramètre d'une fonction à analyser  *)
    (* Paramètre ct : le code TAM des paramètres analysés *)
    (* Génère le code Tam du paramètre à la suite du code TAM déjà écrit *)
    let aux infoast ct =
        match info_ast_to_info infoast with
        |InfoVar (n, typ, dep, reg) -> "LOAD ("^(string_of_int (getTaille typ))^") "
                ^(string_of_int dep)^" ["^reg^"] \n" ^ ct
        |_ -> raise (Failure "erreur interne")
    in List.fold_right aux lparam "" 


(* analyse_code_df : AstPlacement.df -> string *)
(* Paramètre : la fonction à analyser *)
(* Génère le code Tam d'une fonction *)
let analyse_code_df df  = 
    match df with
    | AstPlacement.Fonction(infoast, lparam, li, e) -> 
        begin
            match info_ast_to_info infoast with
            |InfoFun(n,typeRet,ltypeParam,_)-> 
                let cli = analyse_code_bloc li in 
                let cle = analyse_code_expression e in 
                let clparam = analyse_code_param lparam in 
                let r = getTaille typeRet in
                let p = List.fold_right (+) (List.map getTaille ltypeParam) 0 in
                n^"\n"^clparam^cli^cle^"RETURN ("^(string_of_int r)^")"^(string_of_int p)^"\n"
            |_ -> raise (Failure "erreur interne")
        end
    | _ -> ""

(* analyse_code_dfs : AstPlacement.df list -> string *)
(* Paramètre : les fonction à analyser *)
(* Génère le code Tam des fonctions *)
let analyse_code_dfs (AstPlacement.Dfs ldfs) = 
    let nlf = List.map analyse_code_df ldfs in 
    String.concat " " nlf

(* analyser : AstPlacement.Programme -> string *)
(* Paramètre : le programme à analyser *)
(* Génère le code Tam du programme *)
let analyser (AstPlacement.Programme (dfs1, b, dfs2)) = 
    let entete = getEntete() in
    let cdfs1 = analyse_code_dfs dfs1 in
    let cli = analyse_code_bloc b in
    let cdfs2 = analyse_code_dfs dfs2 in
    entete^cdfs1^cdfs2^"main \n "^cli^"HALT"

end
