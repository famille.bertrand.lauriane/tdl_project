open Type
open Ast.AstSyntax


(* Exceptions pour la gestion des identificateurs *)
exception DoubleDeclaration of string 
exception IdentifiantNonDeclare of string 
exception MauvaiseUtilisationIdentifiant of string 

(* Le corps du prototype n'est pas implanté *)
exception PrototypePasImplante

(* Exceptions pour le typage *)
(* Le premier type est le type réel, (le second est le type attendu -> enlevé pour le moment) *)
exception TypeInattendu of typ * typ
exception TypesParametresInattendus of typ list * typ list list
exception TypeBinaireInattendu of binaire * typ * typ (* les types sont les types réels non compatible avec les signatures connues de l'opérateur *)

(* Un pointeur était attendu mais on a une donnée de type typ*)
exception PasUnPointeur of typ



