(* Module de la passe de plaement mémoire *)
module PassePlacementRat : Passe.Passe with type t1 = Ast.AstType.programme and type t2 = Ast.AstPlacement.programme =
struct

  open Tds
  open Type
  open Exceptions
  open Ast
  open AstType
  open AstPlacement

  type t1 = Ast.AstType.programme
  type t2 = Ast.AstPlacement.programme


(* analyse_placement_instruction : AstType.instruction -> int -> string -> AstType.instruction * int *)
(* Paramètre i : l'instruction à analyser *)
(* Paramètre dep : le deplacement à utiliser *)
(* Paramètre reg : le registre à utiliser *)
(* Détermine la place occupée par les instructions dans la mémoire 
et renvoie l'intruction et la taille de celle ci en mémoire *)

let rec analyse_placement_instruction i dep reg = 
    match i with
    | AstType.Declaration (_, info) -> 
        begin
            match info_ast_to_info info with
            | InfoVar (_, t, _, _) -> 
                modifier_adresse_info dep reg info; (* écrire la place de la variable dans l'info ast *)
                (i, getTaille t)
            | _ -> raise (Failure "erreur interne")
        end
    | AstType.Conditionnelle (e, b1, b2) ->
        let nb1 = analyse_placement_bloc b1 dep reg in
        let nb2 = analyse_placement_bloc b2 dep reg in
        (AstType.Conditionnelle (e, nb1, nb2), 0)
    | AstType.TantQue (e, b) -> 
        let nb = analyse_placement_bloc b dep reg in
        (AstType.TantQue (e, nb), 0)
    | _ -> (i, 0)

      
(* analyse_placement_bloc : AstType.bloc -> int -> string -> AstType.bloc *)
(* Paramètre li : liste d'instructions à analyser *)
(* Paramètre dep : le deplacement à utiliser *)
(* Paramètre reg : le registre à utiliser *)
(* Détermine la place occupée par les instructions du bloc dans la mémoire 
et renvoie le bloc *)

and analyse_placement_bloc li dep reg = 
    match li with
    | [] -> []
    | t::q -> 
        let (nt, taille) = analyse_placement_instruction t dep reg in
        let nq = analyse_placement_bloc q (dep + taille) reg in
        nt::nq



(* analyse_placement_parametres : info_ast list -> int *)
(* Paramètre lp : liste de parametres d'une fonction à analyser *)
(* Détermine la place occupée par les paramètres dans la mémoire  *)
let rec analyse_placement_parametres lp =
    match lp with
    | [] -> 0
    | t::q -> 
        let tailleq = analyse_placement_parametres q in
        begin
            match info_ast_to_info t with
            | InfoVar (_, typ, _, _) -> 
                begin
                    modifier_adresse_info (-tailleq-(getTaille typ)) "LB" t;
                    (tailleq+(getTaille typ))
                end
            | _ -> raise (Failure "erreur interne")
        end
        

(* analyse_placement_df : AstType.Df -> AstPlacement.Df *)
(* Paramètre : la fonction à analyser *)
(* Analyse la fonction renvoie un AstPlacement.Df *)
let analyse_placement_df df =
    match df with
    |  AstType.Fonction(info, lparam, li, e) -> 
        let _ = analyse_placement_parametres lparam in
        let nli = analyse_placement_bloc li 3 "LB" in
        AstPlacement.Fonction (info, lparam, nli, e)
    | _ -> AstPlacement.Vide

(* analyse_placement_df : AstType.Dfs -> AstPlacement.Dfs *)
(* Paramètre : la fonction à analyser *)
(* Analyse la fonction renvoie un AstPlacement.Dfs *)
let analyse_placement_dfs (AstType.Dfs ldfs)  = 
    let nlf = List.map analyse_placement_df ldfs in 
    AstPlacement.Dfs(nlf)

(* analyser : AstType.Programme -> AstPlacement.Programme *)
(* Paramètre : le programme à analyser *)
(* Analyse le programme et renvoie un AstPlacement.Programme*)
let analyser (AstType.Programme (dfs1, b, dfs2)) = 
    let ndfs1 = analyse_placement_dfs dfs1 in
    let nb = analyse_placement_bloc b 0 "SB" in
    let ndfs2 = analyse_placement_dfs dfs2 in
    AstPlacement.Programme(ndfs1, nb, ndfs2)

end
