type typ = Bool | Int | Rat | String | Undefined | Pointeur of typ

let rec string_of_type t = 
  match t with
  | Bool ->  "Bool"
  | Int  ->  "Int"
  | Rat  ->  "Rat"
  | String ->  "String" 
  | Undefined -> "Undefined"
  | Pointeur t -> "Pointeur sur " ^ (string_of_type t)


let rec est_compatible t1 t2 =
  match t1, t2 with
  | Bool, Bool -> true
  | Int, Int -> true
  | Rat, Rat -> true
  | String, String -> true 
  | Pointeur t1, Pointeur t2-> est_compatible t1 t2
  | _ -> false 

let est_compatible_list lt1 lt2 =
  try
    List.for_all2 est_compatible lt1 lt2
  with Invalid_argument _ -> false

let getTaille t =
  match t with
  | Int -> 1
  | Bool -> 1
  | Rat -> 2
  | String  ->  1
  | Undefined -> 0
  | Pointeur _ -> 1 
  
 let rec getTaillePointeur t = 
	match t with
	| Pointeur t2 -> getTaillePointeur t2 
	| Int -> 1
	| Bool -> 1
	| Rat -> 2
	| String  ->  1
	| Undefined -> 0
	
 
 
  
