open Compilateur
open PasseTdsRat
open PasseTypeRat
open PassePlacementRat
open Passe

(* Return la liste des adresses des variables d'un programme RAT *)
let getListeDep ratfile =
  let input = open_in ratfile in
  let filebuf = Lexing.from_channel input in
  try
    let ast = Parser.main Lexer.token filebuf in
    let tast = PasseTdsRat.analyser ast in
    let tyast = PasseTypeRat.analyser tast in
    let past = PassePlacementRat.analyser tyast in
    let listeAdresses = PasseVerifPlacement.analyser past in
    listeAdresses
  with
  | Lexer.Error _ as e ->
      report_error ratfile filebuf "lexical error (unexpected character).";
      raise e
  | Parser.Error as e->
      report_error ratfile filebuf "syntax error.";
      raise e

(* teste si dans le fichier fichier, dans la fonction fonction (main pour programme principal)
la occ occurence de la variable var a l'adresse dep[registre]
*)
let test fichier fonction (var,occ) (dep,registre) = 
  let l = getListeDep fichier in
  let lmain = List.assoc fonction l in
  let rec aux i lmain = 
    if i=1 
    then
      let (d,r) = List.assoc var lmain in
      (d=dep && r=registre)
    else 
      aux (i-1) (List.remove_assoc var lmain)
  in aux occ lmain


(*TESTS TP*)

let%test "test1_x" = 
  test "../../fichiersRat/src-rat-placement-test/test1.rat"  "main" ("x",1)  (0,"SB")

let%test "test2_x" = 
  test "../../fichiersRat/src-rat-placement-test/test2.rat"  "main" ("x",1)  (0,"SB")

let%test "test2_y" = 
  test "../../fichiersRat/src-rat-placement-test/test2.rat"  "main" ("y",1) (1,"SB")

let%test "test2_z" = 
  test "../../fichiersRat/src-rat-placement-test/test2.rat"  "main" ("z",1)  (2 ,"SB")

let%test "test3_x" = 
  test "../../fichiersRat/src-rat-placement-test/test3.rat"  "main" ("x",1)  (0, "SB")

let%test "test3_y" = 
  test "../../fichiersRat/src-rat-placement-test/test3.rat"  "main" ("y",1)  (1, "SB")

let%test "test3_z" = 
  test "../../fichiersRat/src-rat-placement-test/test3.rat"  "main" ("z",1)  (2, "SB")

let%test "test4_x" = 
  test "../../fichiersRat/src-rat-placement-test/test4.rat"  "main" ("x",1)  (0, "SB")
  
let%test "test4_y" = 
  test "../../fichiersRat/src-rat-placement-test/test4.rat"  "main" ("y",1)  (2, "SB")
  
let%test "test4_z" = 
  test "../../fichiersRat/src-rat-placement-test/test4.rat"  "main" ("z",1)  (4, "SB")

let%test "test5_x" = 
  test "../../fichiersRat/src-rat-placement-test/test5.rat"  "main" ("x",1)  (0, "SB")
  
let%test "test5_y" = 
  test "../../fichiersRat/src-rat-placement-test/test5.rat"  "main" ("y",1)  (1, "SB")
  
let%test "test5_z" = 
  test "../../fichiersRat/src-rat-placement-test/test5.rat"  "main" ("z",1)  (3, "SB")

let%test "test6_x_1" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("x",1)  (0, "SB")
  
let%test "test6_y_1" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("y",1)  (1, "SB")
  
let%test "test6_z_1" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("z",1)  (3, "SB")

let%test "test6_x_2" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("x",2)  (4, "SB")
  
let%test "test6_y_2" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("y",2)  (5, "SB")
  
let%test "test6_z_2" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("z",2)  (7, "SB")

let%test "test6_x_3" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("x",3)  (4, "SB")
  
let%test "test6_y_3" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("y",3)  (5, "SB")
  
let%test "test6_z_3" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("z",3)  (7, "SB")

let%test "test6_x1" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("x1",1)  (4, "SB")
  
let%test "test6_y1" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("y1",1)  (5, "SB")
  
let%test "test6_z1" = 
  test "../../fichiersRat/src-rat-placement-test/test6.rat"  "main" ("z1",1)  (7, "SB")

  let%test "test7_x_1" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("x",1)  (0, "SB")
    
  let%test "test7_y_1" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("y",1)  (1, "SB")
    
  let%test "test7_z_1" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("z",1)  (3, "SB")
  
  let%test "test7_x_2" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("x",2)  (4, "SB")
    
  let%test "test7_y_2" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("y",2)  (5, "SB")
    
  let%test "test7_z_2" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("z",2)  (7, "SB")
  
  let%test "test7_x1" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("x1",1)  (4, "SB")
    
  let%test "test7_y1" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("y1",1)  (5, "SB")
    
  let%test "test7_z1" = 
    test "../../fichiersRat/src-rat-placement-test/test7.rat"  "main" ("z1",1)  (7, "SB")

  let%test "test8_x_1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("x",1)  (0, "SB")
    
  let%test "test8_y_1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("y",1)  (1, "SB")
    
  let%test "test8_z_1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("z",1)  (3, "SB")
  
  let%test "test8_x_2" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("x",2)  (4, "SB")
    
  let%test "test8_y_2" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("y",2)  (5, "SB")
    
  let%test "test8_z_2" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("z",2)  (7, "SB")
  
  let%test "test8_x1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("x1",1)  (4, "SB")
    
  let%test "test8_y1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("y1",1)  (5, "SB")
    
  let%test "test8_z1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "main" ("z1",1)  (7, "SB")

  let%test "test8_f_x_1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("x",1)  (3, "LB")
    
  let%test "test8_f_y_1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("y",1)  (4, "LB")
    
  let%test "test8_f_z_1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("z",1)  (6, "LB")
  
  let%test "test8_f_x_2" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("x",2)  (7, "LB")
    
  let%test "test8_f_y_2" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("y",2)  (8, "LB")
    
  let%test "test8_f_z_2" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("z",2)  (10, "LB")
  
  let%test "test8_f_x1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("x1",1)  (7, "LB")
    
  let%test "test8_f_y1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("y1",1)  (8, "LB")
    
  let%test "test8_f_z1" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("z1",1)  (10, "LB")
    
  let%test "test8_f_a" = 
    test "../../fichiersRat/src-rat-placement-test/test8.rat"  "f" ("a",1)  (-1, "LB")
    
  let%test "test9_f_a" = 
    test "../../fichiersRat/src-rat-placement-test/test9.rat"  "f" ("a",1)  (-1, "LB")

  let%test "test10_f_a" = 
    test "../../fichiersRat/src-rat-placement-test/test10.rat"  "f" ("a",1)  (-2, "LB")

  let%test "test11_f_a" = 
    test "../../fichiersRat/src-rat-placement-test/test11.rat"  "f" ("a",1)  (-1, "LB")
    
  let%test "test12_f_b" = 
    test "../../fichiersRat/src-rat-placement-test/test12.rat"  "f" ("b",1)  (-4, "LB")
    
  let%test "test12_f_r" = 
    test "../../fichiersRat/src-rat-placement-test/test12.rat"  "f" ("r",1)  (-3, "LB")
    
  let%test "test12_f_i" = 
    test "../../fichiersRat/src-rat-placement-test/test12.rat"  "f" ("i",1)  (-1, "LB") 
  


(*TESTS SUR LES CHAINES DE CARACTERES*)

let%test "test_declaration_chaine" = 
    test "../../tests_projet/tests_chaine/test_declaration_chaine.rat" "main" ("test", 1) (0, "SB")

let%test "test_sous_chaine" = 
    test "../../tests_projet/tests_chaine/test_sous_chaine.rat" "main" ("s", 1) (1, "SB")

let%test "test_concatenation_chaine" = 
    test "../../tests_projet/tests_chaine/test_concatenation_chaine.rat" "main" ("test",1) (2, "SB")
    
let%test "test_longueur_chaine" = 
    test "../../tests_projet/tests_chaine/test_longueur_chaine.rat" "main" ("l", 1) (1, "SB")


(*TESTS SUR LES POINTEURS *)

let%test "test_pointeur_sujet" = 
    test "../../tests_projet/tests_pointeur/test_pointeur_sujet.rat" "main" ("px", 1) (0, "SB")

let%test "test_pointeur_de_pointeur" = 
    test "../../tests_projet/tests_pointeur/test_pointeur_de_pointeur.rat" "main" ("px", 1) (2, "SB")
    
let%test "test_pointeur_de_pointeur2" = 
    test "../../tests_projet/tests_pointeur/test_pointeur_de_pointeur2.rat" "main" ("ppx", 1) (3, "SB")

let%test "test_pointeur_triple" = 
    test "../../tests_projet/tests_pointeur/test_pointeur_triple.rat" "main"  ("pppx", 1) (0, "SB")

let%test "test_adresse" = 
    test "../../tests_projet/tests_pointeur/test_adresse.rat" "main" ("px",1) (2, "SB")


(* TESTS SUR LA SURCHARGE*)
  
  let%test "test_exemple_f_i1" =
	test "../../tests_projet/tests_surcharge/test_exemple.rat" "f" ("i1",1) (-2, "LB")
	
  let%test "test_exemple_f_i2" =
	test "../../tests_projet/tests_surcharge/test_exemple.rat" "f" ("i2",1) (-1, "LB")
	
  let%test "test_exemple_1f_r1" =
	test "../../tests_projet/tests_surcharge/test_exemple.rat" "1f" ("r1",1) (-4, "LB")
	
  let%test "test_exemple_1f_r2" =
	test "../../tests_projet/tests_surcharge/test_exemple.rat" "1f" ("r2",1) (-2, "LB")
	
  let%test "test_exemple_i_1" =
	test "../../tests_projet/tests_surcharge/test_exemple.rat" "main" ("i",1) (0, "SB")
	
  let%test "testGrosseSurcharge_f_i1" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "f" ("i1",1) (-2, "LB")

  let%test "testGrosseSurcharge_f_i2" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "f" ("i2",1) (-1, "LB")
	
  let%test "testGrosseSurcharge_1f_r1" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "1f" ("r1",1) (-3, "LB")
	
  let%test "testGrosseSurcharge_1f_i2" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "1f" ("i2",1) (-1, "LB")
	
  let%test "testGrosseSurcharge_2f_b1" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "2f" ("b1",1) (-2, "LB")
	
  let%test "testGrosseSurcharge_2f_s2" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "2f" ("s2",1) (-1, "LB")
	
  let%test "testGrosseSurcharge_3f_r2" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "3f" ("r1",1) (-4, "LB")
	
  let%test "testGrosseSurcharge_3f_r2" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "3f" ("r2",1) (-2, "LB")
	
  let%test "testGrosseSurcharge_i_1" =
	test "../../tests_projet/tests_surcharge/testGrosseSurcharge.rat" "main" ("i",1) (0, "SB")
	
  let%test "testPlusieursSurcharges_f_i1" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "f" ("i1",1) (-2, "LB")
	
  let%test "testPlusieursSurcharges_f_i2" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "f" ("i2",1) (-1, "LB")
	
  let%test "testPlusieursSurcharges_g_r1" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "g" ("r1",1) (-3, "LB")
	
  let%test "testPlusieursSurcharges_g_i2" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "g" ("i2",1) (-1, "LB")
	
  let%test "testPlusieursSurcharges_1g_b1" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "1g" ("b1",1) (-2, "LB")
	
  let%test "testPlusieursSurcharges_1g_s2" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "1g" ("s2",1) (-1, "LB")
	
  let%test "testPlusieursSurcharges_1f_r1" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "1f" ("r1",1) (-4, "LB")
	
  let%test "testPlusieursSurcharges_1f_r2" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "1f" ("r2",1) (-2, "LB")
	
  let%test "testPlusieursSurcharges_i_1" =
	test "../../tests_projet/tests_surcharge/testPlusieursSurcharges.rat" "main" ("i",1) (0, "SB")
	
  let%test "testSurcharge_f_i1" =
	test "../../tests_projet/tests_surcharge/testSurcharge.rat" "f" ("i1",1) (-2, "LB")
	
  let%test "testSurcharge_f_i2" =
	test "../../tests_projet/tests_surcharge/testSurcharge.rat" "f" ("i2",1) (-1, "LB")
	
  let%test "testSurcharge_1f_i1" =
	test "../../tests_projet/tests_surcharge/testSurcharge.rat" "1f" ("i1",1) (-3, "LB")
	
  let%test "testSurcharge_1f_i2" =
	test "../../tests_projet/tests_surcharge/testSurcharge.rat" "1f" ("i2",1) (-2, "LB")
	
  let%test "testSurcharge_1f_i3" =
	test "../../tests_projet/tests_surcharge/testSurcharge.rat" "1f" ("i3",1) (-1, "LB")
	
  let%test "testSurcharge_i_1" =
	test "../../tests_projet/tests_surcharge/testSurcharge.rat" "main" ("i",1) (0, "SB")

  (* TESTS SUR LES PROTOTYPES *)

  let%test "testFonctionPrototype_f1_a" =
	test "../../tests_projet/tests_prototype/testFonctionPrototype.rat" "f1" ("a",1) (-2, "LB") 

  let%test "testFonctionPrototype_f1_b" =
	test "../../tests_projet/tests_prototype/testFonctionPrototype.rat" "f1" ("b",1) (-1, "LB") 

  let%test "testFonctionPrototype_f2_a" =
	test "../../tests_projet/tests_prototype/testFonctionPrototype.rat" "f2" ("a",1) (-4, "LB") 

  let%test "testFonctionPrototype_f2_b" =
	test "../../tests_projet/tests_prototype/testFonctionPrototype.rat" "f2" ("b",1) (-2, "LB") 

  let%test "testFonctionPrototype_f3_a" =
	test "../../tests_projet/tests_prototype/testFonctionPrototype.rat" "f3" ("a",1) (-4, "LB") 

  let%test "testFonctionPrototype_f3_b" =
	test "../../tests_projet/tests_prototype/testFonctionPrototype.rat" "f3" ("b",1) (-2, "LB")

  let%test "testFonctionPrototype_x_1" =
	test "../../tests_projet/tests_prototype/testFonctionPrototype.rat" "main" ("x",1) (0, "SB")  

  let%test "testFonctionPrototype_y_1" =
	test "../../tests_projet/tests_prototype/testFonctionPrototype.rat" "main" ("y",1) (1, "SB") 
  
  let%test "test_prototype_et_surcharge_add_a" =
	test "../../tests_projet/tests_prototype/test_prototype_et_surcharge.rat" "add" ("a",1) (-2, "LB")

  let%test "test_prototype_et_surcharge_add_b" =
	test "../../tests_projet/tests_prototype/test_prototype_et_surcharge.rat" "add" ("b",1) (-1, "LB")  

  let%test "test_prototype_et_surcharge_1add_a" =
	test "../../tests_projet/tests_prototype/test_prototype_et_surcharge.rat" "1add" ("a",1) (-4, "LB")

  let%test "test_prototype_et_surcharge_1add_a" =
	test "../../tests_projet/tests_prototype/test_prototype_et_surcharge.rat" "1add" ("b",1) (-2, "LB")  

  let%test "test_prototype_et_surcharge_a_1" =
	test "../../tests_projet/tests_prototype/test_prototype_et_surcharge.rat" "main" ("a",1) (0, "SB") 

  let%test "testSujet_f1_a" =
	test "../../tests_projet/tests_prototype/testSujet.rat" "f1" ("a",1) (-2, "LB") 

  let%test "testSujet_f1_b" =
	test "../../tests_projet/tests_prototype/testSujet.rat" "f1" ("b",1) (-1, "LB") 

  let%test "testSujet_f2_a" =
	test "../../tests_projet/tests_prototype/testSujet.rat" "f2" ("a",1) (-4, "LB") 

  let%test "testSujet_f2_b" =
	test "../../tests_projet/tests_prototype/testSujet.rat" "f2" ("b",1) (-2, "LB") 

  let%test "testSujet_f3_a" =
	test "../../tests_projet/tests_prototype/testSujet.rat" "f3" ("a",1) (-4, "LB") 

  let%test "testSujet_f3_b" =
	test "../../tests_projet/tests_prototype/testSujet.rat" "f3" ("b",1) (-2, "LB") 

  let%test "testSujet_x_1" =
	test "../../tests_projet/tests_prototype/testSujet.rat" "main" ("x",1) (0, "SB") 

  let%test "testSujet_y_1" =
	test "../../tests_projet/tests_prototype/testSujet.rat" "main" ("y",1) (1, "SB") 
